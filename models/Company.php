<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $company_name
 * @property string $stock_type
 * @property int $price_entered_date
 * @property string $price
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name', 'price_entered_date', 'price'], 'required'],
            [['stock_type'], 'string'],
            [['price_entered_date'], 'integer'],
            [['company_name', 'price'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => 'Company Name',
            'stock_type' => 'Stock Type',
            'price_entered_date' => 'Price Entered Date',
            'price' => 'Price',
        ];
    }
}
