<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanyMarketsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Markets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-markets-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company Markets', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_item'
    ]) ?>
</div>
