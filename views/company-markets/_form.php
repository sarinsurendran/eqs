<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Company;
use app\models\Markets;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyMarkets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-markets-form">

    <?php $form = ActiveForm::begin(); ?>


    <?=
     $form->field($model, 'company_id')
     ->dropDownList(
            ArrayHelper::map(Company::find()->asArray()->all(), 'id', 'company_name')
            )
   ?>

    <?=
     $form->field($model, 'markets_id')
     ->dropDownList(
            ArrayHelper::map(Markets::find()->asArray()->all(), 'id', 'name')
            )
   ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
