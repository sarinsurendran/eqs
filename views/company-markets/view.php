<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyMarkets */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Markets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-markets-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company.company_name',
            [
            'label'=>'Market Name',
            'value'=>$model->markets->name
            ],
        ],    
    ]) ?>

</div>
