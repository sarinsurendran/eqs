<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Company;
use app\models\Markets;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyMarkets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-markets-form">
<div class="panel panel-default" style="color: #333;
  background-color: #b5d9cb;
  border-color: #ddd;">
    <div class="panel-body">
        <?= Html::a(Html::encode($model->company->company_name), ['view', 'id' => $model->id]); ?>
        <div>
              <?= $model->company->stock_type; ?>
        </div>  
        <div>
              <?= '€:'. $model->company->price; ?>
        </div>  
        <div>
              <?=  $model->markets->name; ?>
        </div>  
  </div>
</div>
    

</div>
