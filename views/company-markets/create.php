<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanyMarkets */

$this->title = 'Create Company Markets';
$this->params['breadcrumbs'][] = ['label' => 'Company Markets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-markets-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
