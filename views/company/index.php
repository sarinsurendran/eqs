<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'company_name',
            'stock_type',
            //'price_entered_date',
            'price',
            [
               'label' => 'Price Entered Date',
               'value' => function ($model) {
                   return date('d-m-Y',$model->price_entered_date);
               }
             ],
             [
               'label' => 'Price Entered Time',
               'value' => function ($model) {
                   return date('H:i:s',$model->price_entered_date);
               }
             ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
